require "kramdown/converter/syntax_highlighter/textmate/version"

module Kramdown
  module Converter
    module SyntaxHighlighter
      module TextMate
        
        def self.call(converter, text, lang, type, call_opts)
          return "<code><span style=\"color: red;\">#{text}</span></code>"
        end

      end
    end

    add_syntax_highlighter(:textmate, SyntaxHighlighter::TextMate)

  end
end
